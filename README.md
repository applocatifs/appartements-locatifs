Ce nouveau quartier immobilier résidentiel d’appartements locatifs à Saint-Nicolas, Lévis, est développé par Immostar. Sur la Rive-sud de Québec, à seulement trois minutes des ponts de Québec, le projet haut de gamme offre des espaces de vie exceptionnels, mêlant modernité, nature et confort.

Address: 1045 Rue Pierre-Perrault, Levis, Quebec, G7A 0Y1

Phone: (418) 956-0009

[Website](http://www.loges.ca)

[
Visit our FACEBOOK page](https://www.facebook.com/logessaintnicolas/)